/*
 * Copyright (C) 2019 Konsulko Group
 * Author: Raquel Medina <raquel.medina@konsulko.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef AGL_IDENTITY_COMMON_H
#define AGL_IDENTITY_COMMON_H

#define _GNU_SOURCE
#define AFB_BINDING_VERSION 3
#include <afb/afb-binding.h>

afb_api_t get_local_api(void);

#endif // AGL_IDENTITY_COMMON_H
